(function() {
    if(lock)return;
    var c = function(e){return document.createElement(e)},
        a = function(e){return document.body.appendChild(e)},
        j = c('script'),
        d = c('script'),
        b = c('div'),
        bo =c('div'),
        s = c('link');
    j.charset = 'utf-8',j.setAttribute('src', 'http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js'),a(j);
    b.id = 'dbody';a(b);
    s.setAttribute('rel', 'stylesheet');s.setAttribute('href', 'http://192.168.186.128/index.css');a(s);
    
    setTimeout(function(){
        d.charset = 'utf-8', d.setAttribute('src', 'http://192.168.186.128/jquery.danmu.js');a(d);
        setTimeout(function(){
            
            $('#dbody').danmu({
                    height: '100%', //区域的高度
                    width: '100%', //区域的宽度
                    speed:10000, //弹幕速度，飞过区域的毫秒数
                    default_font_color:'#000', //弹幕默认字体颜色
                    zindex:'-1',
                } );
            $('#dbody').danmu('danmuStart');
            
            var ws = new WebSocket('ws://192.168.186.128:9502');
            
            ws.onopen   = function (evt) {
                console.log('连接到服务器');
            };
            ws.onclose  = function (evt) {
                console.log('服务器断开连接');
            };
            ws.onmessage= function (evt) {
                var time = $('#dbody').data("nowTime")+1;
                $("#dbody").danmu("addDanmu",{ text:evt.data ,color:"#178bff",size:1,position:0,time:time});
            };
            ws.onerror  = function (evt, e) {
                console.log('Error occured: ' + evt.data);
            };
            
            $('#speak>a').on('click',function(){
                ws.send($('#speak>input').val());
                $('#speak>input').val('');
            });
            $('html').bind('keydown',function(e){
                if(e.keyCode==13){
                    $('#speak>a').click();
                }
            });
            
        },500);
    },500);
    
    document.body.style.marginBottom = '40px';
    bo.id = 'bottom';a(bo);
    bo.innerHTML = '<div id=\"speak\"><input/><a>常言一下</a></div>';
})();
var lock = true;